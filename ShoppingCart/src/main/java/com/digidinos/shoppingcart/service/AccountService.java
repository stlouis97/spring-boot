package com.digidinos.shoppingcart.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shoppingcart.entity.Account;
import com.digidinos.shoppingcart.entity.Product;
import com.digidinos.shoppingcart.repository.AccountRepository;

@Service
public class AccountService {
	@Autowired
	private AccountRepository repo;

	/**
	 * Get all accounts
	 * 
	 * @return List of account
	 */
	public Page<Account> findAll(Pageable pageable) {
		return repo.findAll(pageable);
	}

	public Optional<Account> selectById(int id) {
		Optional<Account> accountOpt = repo.findById(id);
		return accountOpt;
	}

	/**
	 * Find account by name and pagination
	 * 
	 * @param name Name of account
	 */
	public Account findByUsername(String userName) {
		Account account = repo.findByUserName(userName);
		return account;
	}

	public Page<Account> findByAccountName(String name, Pageable pageable) {
		return repo.findByNameAccount(name, pageable);
	}

	/**
	 * Updating account into database
	 * 
	 * @param id      Id of account
	 * @param account entity
	 */
	public void update(Account account) {
		Optional<Account> accountOtp = repo.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdatedAt(new Date());
			account2.setUserName(account.getUserName());
			account2.setUserRole(account.getUserRole());
			account2.setImage(account.getImage());
		}
		this.repo.save(account2);
	}

	/**
	 * Adding account entity into database
	 * 
	 * @param account
	 */
	public void addReservation(Account account) {
		repo.save(account);
	}

	/**
	 * Deleting account from database
	 * 
	 * @param id Id of account
	 */

	public void deleteReservation(int id) {
		Optional<Account> accountOpt = repo.findById(id);
		Account account = accountOpt.get();
		account.setActive(false);
		account.setDeletedAt(new Date());
		repo.save(account);
	}

}
