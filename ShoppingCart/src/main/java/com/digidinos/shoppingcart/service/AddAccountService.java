package com.digidinos.shoppingcart.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shoppingcart.entity.Account;
import com.digidinos.shoppingcart.repository.AccountRepository;
@Service
public class AddAccountService {
	@Autowired
	AccountRepository accountRepository;

	/**
	 * Get all accounts
	 * 
	 * @return List of account
	 */
	public List<Account> selectAll() {
		return this.accountRepository.findAll();
	}
	
	public Page<Account> findAll(Pageable pageable){
		return this.accountRepository.findAll(pageable);
	}
	
	public Optional<Account> selectByID(int id) {

		return accountRepository.findById(id);

	}
	
	/**
	 * Adding account entity into database
	 * 
	 * @param account
	 */
	public void add(Account account) {
		this.accountRepository.save(account);

	}
	/**
	 * Updating account into database
	 * 
	 * @param id      Id of account
	 * @param account entity
	 */
	public void update(Account account) {
		 Optional<Account> accountOtp = accountRepository.findById(account.getId());
	        Account account2 = new Account();
	        if (accountOtp.isPresent()) {
	            account2 = accountOtp.get();
	            account2.setUpdatedAt(new Date());
	            account2.setUserName(account.getUserName());
	            account2.setEncrytedPassword(account.getEncrytedPassword());
	        }
	        this.accountRepository.save(account2);

	}
	
	/**
	 * Deleting account from database
	 * 
	 * @param id Id of account
	 */
	
	public void delete(int id) {
		// TODO Auto-generated method stub
		this.accountRepository.deleteById(id);
	}
	
	/**
	 * Find account by name and pagination
	 * 
	 * @param name Name of account
	 */
	public Account findByUsername(String userName) {
		Account account = accountRepository.findByUserName(userName);
		return account;
	}
	
	/**
	 * Updating account password into database
	 * 
	 * @param id      Id of account
	 * @param account entity
	 */
	public void updatePassword(Account account) {

        Optional<Account> accountOtp =accountRepository.findById(account.getId());
        Account account2 = new Account();
        if (accountOtp.isPresent()) {
            account2 = accountOtp.get();
            account2.setUpdatedAt(new Date());
            account2.setEncrytedPassword(account.getEncrytedPassword());
        }
        this.accountRepository.save(account2);
    }
	
	
	

}
