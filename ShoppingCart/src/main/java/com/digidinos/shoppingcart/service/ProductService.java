package com.digidinos.shoppingcart.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shoppingcart.entity.Product;
import com.digidinos.shoppingcart.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	private ProductRepository repo;

	/**
	 * Get all products
	 * 
	 * @return List of product
	 */
	public List<Product> selectAll() {
		List<Product> products = repo.findAll();

		return products;
	}

	public Page<Product> findAll(Pageable pageable) {
		return repo.findAll(pageable);
	}

	public List<Product> top5NewProduct() {
		List<Product> products = repo.top5ProductNew();
		return products;
	}

	/**
	 * Get Reservation entity by Id
	 * 
	 * @param id Id of product
	 * @return Reservation entity
	 */
	public Optional<Product> selectById(int id) {
		return repo.findById(id);
	}

	/**
	 * Get Reservation entity by Id
	 * 
	 * @param id Id of product
	 * @return Reservation entity
	 */
	public List<Product> selectByPrice(int size) {
		return repo.findByPrice(size);
	}

	/**
	 * Adding product entity into database
	 * 
	 * @param product
	 */
	public void addReservation(Product product) {
		repo.save(product);
	}

	/**
	 * Updating product into database
	 * 
	 * @param id      Id of product
	 * @param product entity
	 */
	public void updateReservation(Product product) {
		repo.save(product);
	}

	/**
	 * Deleting product from database
	 * 
	 * @param id Id of product
	 */
	public void deleteReservation(int id) {
		Optional<Product> productOpt = selectById(id);
		Product product = productOpt.get();
		product.setDelete(true);
		product.setDeletedAt(new Date());
		repo.save(product);
	}

	/**
	 * Find product by name and pagination
	 * 
	 * @param name Name of product
	 */
	public Page<Product> findByNameProduct(String name, Pageable pageable) {
		Page<Product> products = repo.findByNameProduct(name, pageable);
		return products;
	}

	public List<Product> findByName(String name) {
		List<Product> products = repo.findByName(name);
		return products;
	}

	/**
	 * Update product image
	 * 
	 * @param id Id of product
	 */
	public void updateImage(Product product) {
		Optional<Product> productOtp = repo.findById(product.getId());
		Product product2 = new Product();
		if (productOtp.isPresent()) {
			product2 = productOtp.get();
			product2.setUpdatedAt(new Date());
			product2.setCode(product.getCode());
			product2.setName(product.getName());
			product2.setPrice(product.getPrice());
			product2.setImage(product.getImage());
		}
		this.repo.save(product2);
	}
}
