package com.digidinos.shoppingcart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidinos.shoppingcart.entity.OrderDetail;
import com.digidinos.shoppingcart.repository.OrderDetailRepository;
@Service
public class OrderDetailService {
	@Autowired
	private OrderDetailRepository repo;
	
	/**
	 * Get order details
	 * 
	 * @return order details
	 */
	
	public Optional<OrderDetail> selectById(int id) {
		Optional<OrderDetail> orderDetail = repo.findById(id);
		return orderDetail;
	}
	
	/**
	 * Get list order details
	 * 
	 * @return list order detail
	 */
	public List<OrderDetail> findAllByOrderId(Integer orderID){
		List<OrderDetail> listOrderDetail = repo.findAllByOrderId(orderID);
		return listOrderDetail;
	}
}
