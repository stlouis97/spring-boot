package com.digidinos.shoppingcart.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shoppingcart.entity.Order;
import com.digidinos.shoppingcart.entity.OrderDetail;
import com.digidinos.shoppingcart.entity.Product;
import com.digidinos.shoppingcart.model.CartInfo;
import com.digidinos.shoppingcart.model.CartLineInfo;
import com.digidinos.shoppingcart.model.CustomerInfo;
import com.digidinos.shoppingcart.repository.OrderDetailRepository;
import com.digidinos.shoppingcart.repository.OrderRepository;
import com.digidinos.shoppingcart.repository.ProductRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private OrderDetailRepository orderDetailRepository;

	
	/**
	 * Get all orders
	 * 
	 * @return List of orders
	 */
	public List<Order> findAll() {
		List<Order> orders = orderRepository.findAll();
		return orders;
	}

	public Optional<Order> findById(int id) {
		Optional<Order> order = orderRepository.findById(id);
		return order;
	}

	public Page<Order> findByCustomerName(String name, Pageable pageable) {
		return orderRepository.findByCustomerName(name, pageable);
	}

	public Page<Order> findAll(Pageable pageable) {
		return orderRepository.findAll(pageable);
	}

	public List<Order> findAllById(Iterable<Integer> id) {
		List<Order> order = orderRepository.findAllById(id);
		return order;
	}
	
	/**
	 * Adding order entity into database
	 * 
	 * @param order
	 */

	public void addOrder(CartInfo cartInfo) {
		int orderNum = orderRepository.maxOrderNum() + 1;
		System.out.println(orderNum);
		Order order = new Order();
		order.setOrderNum(orderNum);
		order.setOrderDate(new Date());
		order.setAmount(cartInfo.getAmountTotal());

		CustomerInfo customerInfo = cartInfo.getCustomerInfo();
		order.setCustomerName(customerInfo.getName());
		order.setCustomerEmail(customerInfo.getEmail());
		order.setCustomerPhone(customerInfo.getPhone());
		order.setCustomerAddress(customerInfo.getAddress());
		System.out.println("Name : " + order.getCustomerName());

		orderRepository.save(order);

		List<CartLineInfo> lines = cartInfo.getCartLines();

		for (CartLineInfo line : lines) {
			OrderDetail detail = new OrderDetail();
			detail.setOrder(order);
			detail.setAmount(line.getAmount());
			detail.setPrice(line.getProductInfo().getPrice());
			detail.setQuanity(line.getQuantity());

			int id = line.getProductInfo().getId();
			Optional<Product> productOpt = productRepository.findById(id);
			Product product = productOpt.get();
			detail.setProduct(product);
			orderDetailRepository.save(detail);
		}
	}
}
