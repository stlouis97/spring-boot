package com.digidinos.shoppingcart.form;

import java.io.IOException;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import com.digidinos.shoppingcart.entity.Account;

public class AccountChangeRoleForm {
	private int id;
	private String userName;
	private boolean active = true;
	private String userRole;
	private boolean newAccount = false;
	private boolean isDelete = true;

	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	
	public AccountChangeRoleForm() {
		this.newAccount = true;
	}

	public AccountChangeRoleForm(Account account) {
		this.id = account.getId();
		this.active = account.isActive();
		this.userName = account.getUserName();
		this.userRole = account.getUserRole();
		this.isDelete = account.isDelete();
//		this.encrytedPassword = account.getEncrytedPassword();
	}

	public AccountChangeRoleForm(int id, String userName, boolean active, String userRole, boolean newAccount, boolean isDelete,
			String encrytedPassword) {
		super();
		this.id = id;
		this.userName = userName;
		this.active = active;
		this.userRole = userRole;
		this.newAccount = newAccount;
		this.isDelete = isDelete;
//		this.encrytedPassword = encrytedPassword;
	}

	public Account getAccount() throws IOException {

		Account account = new Account();
		account.setId(this.id);
		account.setUserName(this.userName);
		account.setUserRole(this.userRole);
//		if(this.encrytedPassword!=null) {
//			account.setEncrytedPassword(bCryptPasswordEncoder.encode(this.encrytedPassword));
//		}

		account.setDelete(false);
		account.setActive(true);

		return account;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public boolean isNewAccount() {
		return newAccount;
	}

	public void setNewAccount(boolean newAccount) {
		this.newAccount = newAccount;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
//
//	public String getEncrytedPassword() {
//		return encrytedPassword;
//	}
//
//	public void setEncrytedPassword(String encrytedPassword) {
//		this.encrytedPassword = encrytedPassword;
//	}


	

}
