package com.digidinos.shoppingcart.form;

import java.io.IOException;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.digidinos.shoppingcart.entity.Product;

public class ProductForm {
	private int id;
	private String code;
	private String name;
	private double price;
	private boolean isDelete;
	private byte[] image;
	private boolean newProduct = false;

	// Upload file.
	private MultipartFile fileData;

	public ProductForm() {
		this.newProduct = true;
	}

	public ProductForm(Product product) {
		this.id = product.getId();
		this.code = product.getCode();
		this.name = product.getName();
		this.price = product.getPrice();
		this.isDelete = product.isDelete();
		this.image = product.getImage();
	}
	public Product getProduct( ) throws IOException {
		Product product = new Product();
    	product.setId(this.id);
    	product.setCode(this.code);
    	product.setName(this.name);
    	product.setPrice(this.price);
    	product.setDelete(false);  
    	product.setCreatedAt(new Date());
    	product.setUpdatedAt(new Date());
    	if(this.getFileData().isEmpty()) {
    		product.setImage(image);
    	}else {
    		product.setImage(this.getFileData().getBytes());
    	}
    	return product;
    }
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public MultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(MultipartFile fileData) {
		this.fileData = fileData;
	}

	public boolean isNewProduct() {
		return newProduct;
	}

	public void setNewProduct(boolean newProduct) {
		this.newProduct = newProduct;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	
}
