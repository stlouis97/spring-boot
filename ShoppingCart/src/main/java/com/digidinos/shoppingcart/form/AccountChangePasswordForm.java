package com.digidinos.shoppingcart.form;

import java.io.IOException;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.digidinos.shoppingcart.entity.Account;

public class AccountChangePasswordForm {
	private int id;
	private String encrytedPassword;
	private String verifyPassword;
	private boolean newPassword;

	public AccountChangePasswordForm() {
		this.newPassword = true;
	}

	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	public AccountChangePasswordForm(Account account) {
		this.id = account.getId();
		this.encrytedPassword = account.getEncrytedPassword();
	}

	public Account getAccount() throws IOException {
		Account account = new Account();
		account.setId(this.id);
		if (this.encrytedPassword != null) {
			account.setEncrytedPassword(passwordEncoder.encode(this.encrytedPassword));
		}
		return account;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isNewPassword() {
		return newPassword;
	}

	public void setNewPassword(boolean newPassword) {
		this.newPassword = newPassword;
	}

	public String getVerifyPassword() {
		return verifyPassword;
	}

	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}

	public String getEncrytedPassword() {
		return encrytedPassword;
	}

	public void setEncrytedPassword(String encrytedPassword) {
		this.encrytedPassword = encrytedPassword;
	}
}
