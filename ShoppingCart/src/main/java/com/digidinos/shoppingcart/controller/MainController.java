package com.digidinos.shoppingcart.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shoppingcart.entity.Product;
import com.digidinos.shoppingcart.form.CustomerForm;
import com.digidinos.shoppingcart.model.CartInfo;
import com.digidinos.shoppingcart.model.CustomerInfo;
import com.digidinos.shoppingcart.model.ProductInfo;
import com.digidinos.shoppingcart.pagination.Pagination;
import com.digidinos.shoppingcart.service.OrderService;
import com.digidinos.shoppingcart.service.ProductService;
import com.digidinos.shoppingcart.utils.Utils;
import com.digidinos.shoppingcart.validator.CustomerFormValidator;

@Controller
@Transactional
public class MainController {

	@Autowired
	private ProductService productService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private CustomerFormValidator customerFormValidator;

	@Autowired
	private Pagination pagination;

	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		// Trường hợp update SL trên giỏ hàng.
		// (@ModelAttribute("cartForm") @Validated CartInfo cartForm)
		if (target.getClass() == CartInfo.class) {

		}

		// Trường hợp save thông tin khách hàng.
		// (@ModelAttribute @Validated CustomerInfo customerForm)
		else if (target.getClass() == CustomerForm.class) {
			dataBinder.setValidator(customerFormValidator);
		}

	}

	@RequestMapping("/403")
	public String accessDenied() {
		return "/403";
	}
	//GET: Hiển thị trang Index
	@RequestMapping(value = { "/", "/home" })
	public String home(Model model, @RequestParam(value = "name", required = false, defaultValue = "") String likename,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "9") int size) {
		List<Product> top5Product = productService.top5NewProduct();
		model.addAttribute("top5Products", top5Product);
		List<String> pagingNumber = new ArrayList();

		if (!likename.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> productPage = productService.findByNameProduct(likename, pageable);
			pagingNumber = pagination.paginationProduct(productPage, currentPage);
			model.addAttribute("searchName", likename);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationProducts", productPage);
			return "index";
		}
		model.addAttribute("searchName", likename);
		Pageable pageable = PageRequest.of(currentPage - 1, size);
		Page<Product> productPage = productService.findAll(pageable);
		pagingNumber = pagination.paginationProduct(productPage, currentPage);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationProducts", productPage);
		return "index";

	}
	
	//GET: Hiển thị trang Product List
	@RequestMapping({ "/productList" })
	public String listProductHandler(Model model,
			@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "18") int size) {
		List<Product> top5Product = productService.top5NewProduct();
		model.addAttribute("top5Products", top5Product);
		List<String> pagingNumber = new ArrayList();

		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> productPage = productService.findByNameProduct(likeName, pageable);
			pagingNumber = pagination.paginationProduct(productPage, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationProducts", productPage);
			return "productList";
		}
		Pageable pageable = PageRequest.of(currentPage - 1, size);
		Page<Product> productPage = productService.findAll(pageable);
		pagingNumber = pagination.paginationProduct(productPage, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationProducts", productPage);
		return "productList";
	}
	
	//Mua sản phẩm
	@RequestMapping({ "/buyProduct" })
	public String listProductHandler(HttpServletRequest request, Model model, //
			@RequestParam(value = "id", defaultValue = "") Integer id) {

		Product product = null;
		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			if (productOpt.isPresent()) {
				product = productOpt.get();
			}
		}
		if (product != null) {

			//
			CartInfo cartInfo = Utils.getCartInSession(request);

			ProductInfo productInfo = new ProductInfo(product);

			cartInfo.addProduct(productInfo, 1);
		}

		return "redirect:/shoppingCart";
	}
	
	//Xóa sản phẩm khỏi giỏ hàng
	@RequestMapping({ "/shoppingCartRemoveProduct" })
	public String removeProductHandler(HttpServletRequest request, Model model, //
			@RequestParam(value = "id", defaultValue = "") Integer id) {
		Product product = null;
		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			if (productOpt.isPresent()) {
				product = productOpt.get();
			}
		}
		if (product != null) {

			CartInfo cartInfo = Utils.getCartInSession(request);

			ProductInfo productInfo = new ProductInfo(product);

			cartInfo.removeProduct(productInfo);

		}

		return "redirect:/shoppingCart";
	}

//
	// POST: Cập nhập số lượng cho các sản phẩm đã mua.
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
	public String shoppingCartUpdateQty(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("cartForm") CartInfo cartForm) {

		CartInfo cartInfo = Utils.getCartInSession(request);
		cartInfo.updateQuantity(cartForm);

		return "redirect:/shoppingCart";
	}

	// GET: Hiển thị giỏ hàng.
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
	public String shoppingCartHandler(HttpServletRequest request, Model model) {
		CartInfo myCart = Utils.getCartInSession(request);

		model.addAttribute("cartForm", myCart);
		return "shoppingCart";
	}

	// GET: Nhập thông tin khách hàng.
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.GET)
	public String shoppingCartCustomerForm(HttpServletRequest request, Model model) {

		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		}
		CustomerInfo customerInfo = cartInfo.getCustomerInfo();

		CustomerForm customerForm = new CustomerForm(customerInfo);

		model.addAttribute("customerForm", customerForm);

		return "shoppingCartCustomer";
	}

	// POST: Save thông tin khách hàng.
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.POST)
	public String shoppingCartCustomerSave(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("customerForm") @Validated CustomerForm customerForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			customerForm.setValid(false);
			// Forward tới trang nhập lại.
			return "shoppingCartCustomer";
		}

		customerForm.setValid(true);
		CartInfo cartInfo = Utils.getCartInSession(request);
		CustomerInfo customerInfo = new CustomerInfo(customerForm);
		cartInfo.setCustomerInfo(customerInfo);

		return "redirect:/shoppingCartConfirmation";
	}

	// GET: Xem lại thông tin để xác nhận.
	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
	public String shoppingCartConfirmationReview(HttpServletRequest request, Model model) {
		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo == null || cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		} else if (!cartInfo.isValidCustomer()) {

			return "redirect:/shoppingCartCustomer";
		}
		model.addAttribute("myCart", cartInfo);

		return "shoppingCartConfirmation";
	}

	// POST: Gửi đơn hàng (Save).
	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.POST)

	public String shoppingCartConfirmationSave(HttpServletRequest request, Model model) {
		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		} else if (!cartInfo.isValidCustomer()) {

			return "redirect:/shoppingCartCustomer";
		}
		try {
			orderService.addOrder(cartInfo);
		} catch (Exception e) {

			return "shoppingCartConfirmation";
		}

		// Xóa giỏ hàng khỏi session.
		Utils.removeCartInSession(request);

		// Lưu thông tin đơn hàng cuối đã xác nhận mua.
		Utils.storeLastOrderedCartInSession(request, cartInfo);

		return "redirect:/shoppingCartFinalize";
	}
	
	
	//GET: Thông báo mua hàng thành công
	@RequestMapping(value = { "/shoppingCartFinalize" }, method = RequestMethod.GET)
	public String shoppingCartFinalize(HttpServletRequest request, Model model) {

		CartInfo lastOrderedCart = Utils.getLastOrderedCartInSession(request);

		if (lastOrderedCart == null) {
			return "redirect:/shoppingCart";
		}
		model.addAttribute("lastOrderedCart", lastOrderedCart);
		return "shoppingCartFinalize";
	}

	@RequestMapping(value = { "/productImage" }, method = RequestMethod.GET)
	public void productImage(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("id") Integer id) throws IOException {
		Product product = null;
		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			if (productOpt.isPresent()) {
				product = productOpt.get();
			}
		}
		if (product != null && product.getImage() != null) {
			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
			response.getOutputStream().write(product.getImage());
		}
		response.getOutputStream().close();
	}

}
