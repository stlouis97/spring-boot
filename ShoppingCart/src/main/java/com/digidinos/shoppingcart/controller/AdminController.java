package com.digidinos.shoppingcart.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shoppingcart.entity.Account;
import com.digidinos.shoppingcart.entity.Order;
import com.digidinos.shoppingcart.entity.OrderDetail;
import com.digidinos.shoppingcart.entity.Product;
import com.digidinos.shoppingcart.form.AccountChangePasswordForm;
import com.digidinos.shoppingcart.form.AccountChangeRoleForm;
import com.digidinos.shoppingcart.form.AccountForm;
import com.digidinos.shoppingcart.form.AddAccountForm;
import com.digidinos.shoppingcart.form.ProductForm;
import com.digidinos.shoppingcart.model.OrderDetailInfo;
import com.digidinos.shoppingcart.model.OrderInfo;
import com.digidinos.shoppingcart.pagination.Pagination;
import com.digidinos.shoppingcart.service.AccountService;
import com.digidinos.shoppingcart.service.AddAccountService;
import com.digidinos.shoppingcart.service.OrderDetailService;
import com.digidinos.shoppingcart.service.OrderService;
import com.digidinos.shoppingcart.service.ProductService;
import com.digidinos.shoppingcart.validator.AccountChangePasswordValidator;
import com.digidinos.shoppingcart.validator.AddAccountValidator;
import com.digidinos.shoppingcart.validator.ProductFormValidator;

@Controller
@Transactional
public class AdminController {
	
	@Autowired
	private Pagination pagination;
	
	@Autowired
	private AddAccountService accountService1;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductFormValidator productFormValidator;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private AccountChangePasswordValidator accountFormValidator;
	
	@Autowired
	private AddAccountValidator addAccountValidator;
	
	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		if (target.getClass() == ProductForm.class) {
			dataBinder.setValidator(productFormValidator);
		}
		if (target.getClass() == AccountChangePasswordForm.class) {
			dataBinder.setValidator(accountFormValidator);
		}
		if (target.getClass() == AddAccountForm.class) {
			dataBinder.setValidator(addAccountValidator);
		}

	}

	// GET: Hiển thị trang login
	@RequestMapping(value = { "/admin/login" }, method = RequestMethod.GET)
	public String login(Model model) {

		return "login";
	}
	//GET: Hiển thị Profile
	@RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.GET)
	public String accountInfo(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AddAccountForm accountForm = null;
		Account account = accountService.findByUsername(userDetails.getUsername());
		accountForm = new AddAccountForm(account);
		System.out.println(userDetails.getPassword());
		System.out.println(userDetails.getUsername());
		System.out.println(userDetails.isEnabled());
		model.addAttribute("accountForm",accountForm);
		model.addAttribute("userDetails", userDetails);
		return "accountInfo";
	}
	// GET : Hiển thị list Product
		@RequestMapping(value = { "/admin/productManager" }, method = RequestMethod.GET)
		public String listProductHandler(Model model,
				@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
				@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
				@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
			List<String> pagingNumber = new ArrayList();
//			Sort sortable = Sort.by("id").descending();
			if (!likeName.isEmpty()) {
				Pageable pageable = PageRequest.of(currentPage - 1, size);
				Page<Product> pageProduct = productService.findByNameProduct(likeName, pageable);
				pagingNumber = pagination.paginationProduct(pageProduct, currentPage);
				model.addAttribute("searchName", likeName);
				model.addAttribute("pagination", pagingNumber);
				model.addAttribute("paginationProducts", pageProduct);
				return "productManager";
			}
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> productPage = productService.findAll(pageable);
			pagingNumber = pagination.paginationProduct(productPage, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationProducts", productPage);
			return "productManager";
		}

	// GET : Hiển thị Order List
		@RequestMapping(value = { "/admin/orderList" }, method = RequestMethod.GET)
		public String orderList(Model model,
				@RequestParam(value = "name", required = false,defaultValue = "") String likeName,
				@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
				@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
			List<String> pagingNumber = new ArrayList();
//			Sort sortable = Sort.by("id").descending();
			if (!likeName.isEmpty()) {
				Pageable pageable = PageRequest.of(currentPage - 1, size);
				Page<Order> pageProduct = orderService.findByCustomerName(likeName, pageable);
				pagingNumber = pagination.paginationOrder(pageProduct, currentPage);
				model.addAttribute("searchName", likeName);
				model.addAttribute("pagination", pagingNumber);
				model.addAttribute("paginationOrders", pageProduct);
				return "orderList";
			}
			Pageable pageable = PageRequest.of(currentPage-1, size);
			Page<Order> pageOrder = orderService.findAll(pageable);
			pagingNumber = pagination.paginationOrder(pageOrder, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationOrders", pageOrder);
			return "orderList";
		}

//	// GET: Hiển thị product
	@RequestMapping(value = { "/admin/product" }, method = RequestMethod.GET)
	public String product(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		ProductForm productForm = null;

		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			if (productOpt.isPresent()) {
				Product product = productOpt.get();
				productForm = new ProductForm(product);
			}
		}
		if (productForm == null) {
			productForm = new ProductForm();
			productForm.setNewProduct(true);
		}
		model.addAttribute("productForm", productForm);
		return "product";
	}

//	// POST: Save product
	@RequestMapping(value = { "/admin/product" }, method = RequestMethod.POST)
	public String productSave(Model model, //
			@ModelAttribute("productForm") @Validated ProductForm productForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "product";
		}
		try {
            if(productForm.getFileData().isEmpty()) {
                productService.updateImage(productForm.getProduct());
            }
            productService.addReservation(productForm.getProduct());
        } catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "product";
		}

		return "redirect:/admin/productManager";
	}
	//GET: Xóa product
	@RequestMapping(value = { "admin/deleteProduct" }, method = RequestMethod.GET)
	public String removeProductHandler(HttpServletRequest request, Model model, //
			@RequestParam(value = "id", defaultValue = "") Integer id) {
		if (id != null) {
			productService.deleteReservation(id);
		}
		return "redirect:productManager";

	}
	
	//GET: Hiển thị thông tin order
	@RequestMapping(value = { "/admin/order" }, method = RequestMethod.GET)
	public String orderView(Model model, @RequestParam("orderId") Integer orderId) {
		OrderInfo orderInfo = null;
		if (orderId != null) {
			Optional<Order> orderOpt = orderService.findById(orderId);
			if (orderOpt.isPresent()) {
				Order order = orderOpt.get();
				orderInfo = new OrderInfo(order.getId(), order.getOrderDate(), order.getOrderNum(),order.getAmount(), order.getCustomerName(), order.getCustomerAddress(), order.getCustomerEmail(), order.getCustomerPhone());
			}
		}
		if (orderInfo == null) {
			return "redirect:/admin/orderList";
		}
		List<OrderDetailInfo> listOrderDetailInfo = new ArrayList<>();
		List<OrderDetail> listOrderDetails = orderDetailService.findAllByOrderId(orderId);
		for(OrderDetail orderDetail : listOrderDetails) {
			OrderDetailInfo orderDetailInfo = new OrderDetailInfo(orderDetail.getId(), orderDetail.getProduct().getCode(), orderDetail.getProduct().getName(), orderDetail.getQuanity(), orderDetail.getPrice(), orderDetail.getAmount());
			listOrderDetailInfo.add(orderDetailInfo);
		}
		orderInfo.setDetails(listOrderDetailInfo);
		

		model.addAttribute("orderInfo", orderInfo);

		return "order";
	}
	
	//Hiển thị List account
	@RequestMapping(value = { "/admin/accountList" }, method = RequestMethod.GET)
	public String listUserHandler(Model model,
			@RequestParam(value = "name", required = false,defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Account> pageAccount = accountService.findByAccountName(likeName, pageable);
			pagingNumber = pagination.paginationAccount(pageAccount, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationUsers", pageAccount);
			return "accountList";
		}
		Pageable pageable = PageRequest.of(currentPage-1, size);
		Page<Account> pageAccount = accountService.findAll(pageable);
		pagingNumber = pagination.paginationAccount(pageAccount, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationUsers", pageAccount);
		return "accountList";
	}
	
	//GET: Lấy thông tin account
	@RequestMapping(value = { "/admin/editAccount" }, method = RequestMethod.GET)
	public String account(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountChangeRoleForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById (id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AccountChangeRoleForm(account);
			}
		}

		if (accountForm == null) {
			accountForm = new AccountChangeRoleForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm", accountForm);
		return "editAccount";
	}
	
	//POST: Update account
	@RequestMapping(value = { "/admin/editAccount" }, method = RequestMethod.POST)
	public String accountSave(Model model, //
			@ModelAttribute("accountForm") @Validated AccountChangeRoleForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "editAccount";
		}
		try {
			accountService.update(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "editAccount";
		}

		return "redirect:/admin/accountList";
	}
	
	//GET: Xóa Account
	@RequestMapping(value = { "admin/deleteAccount" }, method = RequestMethod.GET)
	public String removeAccountHandler(HttpServletRequest request, Model model, //
			@RequestParam(value = "id", defaultValue = "") Integer id) {
		if (id != null) {
			accountService.deleteReservation(id);
		}
		return "redirect:accountList";

	}
	
	//GET: Thêm mới account
	@RequestMapping(value = { "/admin/addAccount" }, method = RequestMethod.GET)
	public String addAccount(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AddAccountForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService1.selectByID(id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AddAccountForm(account);
			}
		}

		if (accountForm == null) {
			accountForm = new AddAccountForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm1", accountForm);
		return "addAccount";
	}
	
	//POST: Lưu account
	@RequestMapping(value = { "/admin/addAccount" }, method = RequestMethod.POST)
	public String addAccountSave(Model model, //
			@ModelAttribute("accountForm1") @Validated AddAccountForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "addAccount";
		}
		try {
			System.out.println(accountForm.getAccount());
			accountService1.add(accountForm.getAccount());;
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "addAccount";
		}

		return "redirect:/admin/accountList";
	}
	
	
	//GET: Thay đổi mật khẩu
	@RequestMapping(value = { "/admin/passwordChange" }, method = RequestMethod.GET)
	public String editPass(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountChangePasswordForm accountChangeForm = null;
		Account account = accountService1.findByUsername(userDetails.getUsername());
		accountChangeForm = new AccountChangePasswordForm(account);
		accountChangeForm.setNewPassword(true);
		model.addAttribute("accountChangePasswordForm", accountChangeForm);
		return "passwordChange";
	}
	
	//POST: Lưu mật khẩu
	@RequestMapping(value = { "/admin/passwordChange" }, method = RequestMethod.POST)
	public String savePass(Model model, //
			@ModelAttribute("accountChangePasswordForm") @Validated AccountChangePasswordForm accountChangePasswordForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
			return "passwordChange";
		}
		try {

			accountService1.updatePassword(accountChangePasswordForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "passwordChange";
		}
		return "redirect:accountInfo";
	}
	
	
	//GET: Update ảnh profile
	@RequestMapping(value = { "/admin/editInfo" }, method = RequestMethod.GET)
	public String editInfo(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById (id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AccountForm(account);
			}
		}

		if (accountForm == null) {
			accountForm = new AccountForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm", accountForm);
		return "editInfo";
	}
	
	//POST : Lưu ảnh profile
	@RequestMapping(value = { "/admin/editInfo" }, method = RequestMethod.POST)
	public String saveInfo(Model model, //
			@ModelAttribute("accountForm") @Validated AccountForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "editInfo";
		}
		try {
			System.out.println(accountForm.getAccount());
			accountService.update(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "editInfo";
		}

		return "redirect:/admin/accountInfo";
	}

	@RequestMapping(value = { "/accountImage" }, method = RequestMethod.GET)
	public void accountImage(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("id") Integer id) throws IOException {
		Account account = null;
		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById(id);
			if (accountOpt.isPresent()) {
				account = accountOpt.get();
			}
		}
		if (account != null && account.getImage() != null) {
			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
			response.getOutputStream().write(account.getImage());
		}
		response.getOutputStream().close();
	}
}
