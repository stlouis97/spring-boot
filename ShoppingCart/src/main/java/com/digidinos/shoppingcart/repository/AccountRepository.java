package com.digidinos.shoppingcart.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digidinos.shoppingcart.entity.Account;

@Repository
public interface AccountRepository extends BaseRepository<Account, Integer> {
	
	
	/**
	 * Find account by name 
	 * 
	 * @param name Name of account
	 */
	
	Account findByUserName(String userName);

	@Query(value = "select * FROM accounts WHERE user_name LIKE  %:name%", nativeQuery = true)
	Page<Account> findByNameAccount(@Param("name") String name, Pageable pageable);

}
