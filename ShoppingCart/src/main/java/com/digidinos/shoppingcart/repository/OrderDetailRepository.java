package com.digidinos.shoppingcart.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.digidinos.shoppingcart.entity.OrderDetail;

@Repository
public interface OrderDetailRepository extends BaseRepository<OrderDetail, Integer> {
	
	/**
	 * Find order detail by order id
	 * 
	 * 
	 * @return list order detail
	 */
	@Query(value="SELECT * FROM Order_Details o where o.order_id = :orderId", nativeQuery = true) 
	List<OrderDetail> findAllByOrderId(Integer orderId);
}
