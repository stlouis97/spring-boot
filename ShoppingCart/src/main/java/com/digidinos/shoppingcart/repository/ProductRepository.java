package com.digidinos.shoppingcart.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digidinos.shoppingcart.entity.Product;

@Repository
public interface ProductRepository extends BaseRepository<Product, Integer> {

	/**
	 * Find product by price
	 * 
	 * @param price Price of product
	 * @return list product
	 */
	@Query(value = "SELECT t.* FROM products t where t.price = :price and is_delete=0", nativeQuery = true)
	List<Product> findByPrice(@Param("price") int price);

	/**
	 * Select top 5 latest product
	 * 
	 * @return list product
	 */
	@Query(value = "select  id, image, created_at , deleted_at , updated_at , code , is_delete , name , price ,description from products where is_delete = 0 order by created_at desc limit 5", nativeQuery = true)
	List<Product> top5ProductNew();

	/**
	 * Find product by name and paging
	 * 
	 * @param name name of product
	 * @return list product
	 */
	@Query(value = "select * FROM products WHERE NAME LIKE  %:name% and is_delete=0", nativeQuery = true)
	Page<Product> findByNameProduct(@Param("name") String name, Pageable pageable);

	/**
	 * Find product by name
	 * 
	 * @param name name of product
	 * @return list product
	 */
	@Query(value = "select * FROM products WHERE NAME LIKE  %:name% and is_delete=0", nativeQuery = true)
	List<Product> findByName(@Param("name") String name);

}
