package com.digidinos.shoppingcart.pagination;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.digidinos.shoppingcart.entity.Account;
import com.digidinos.shoppingcart.entity.Order;
import com.digidinos.shoppingcart.entity.Product;
@Component
public class Pagination {
 
	int currentPage;
	int range = 5;
	int totalPages;
	int start = 1;

	public List<String> paginationProduct(Page<Product> productPage, int currentPage) {
		int delta = 2;
		int left = currentPage - delta;
		int right = currentPage + delta + 1;
		int l = 0;
		List<String> pagingNumber = new ArrayList<String>();
		int totalPages = productPage.getTotalPages();
		List<String> pagingWithDots = new ArrayList<String>();
		
		if (currentPage == 1) {
			pagingWithDots.add("Previous");
		} else {
			pagingWithDots.add(String.valueOf(currentPage - 1));
		}
		
		for (int i = 1; i <= totalPages; i++) {
			if (i == 1 || i == totalPages || i >= left && i < right) {
				pagingNumber.add(String.valueOf(i));
			}
		}
		for (String i : pagingNumber) {
			if (l > 0) {
				if (Integer.parseInt(i) - l == 2) {
					pagingWithDots.add(String.valueOf(l + 1));
				} else if (Integer.parseInt(i) - l != 1) {
					pagingWithDots.add("...");
				}
			}
			pagingWithDots.add(i);
			l = Integer.parseInt(i);
		}
		if (currentPage == totalPages) {
			pagingWithDots.add("Next");
		} else {
			pagingWithDots.add(String.valueOf(currentPage + 1));
		}

		return pagingWithDots;
	}

	public List<String> paginationAccount(Page<Account> accountPage, int currentPage) {
		int delta = 2;
		int left = currentPage - delta;
		int right = currentPage + delta + 1;
		int l = 0;
		List<String> pagingNumber = new ArrayList<String>();
		int totalPages = accountPage.getTotalPages();
		List<String> pagingWithDots = new ArrayList<String>();
		
		if (currentPage == 1) {
			pagingWithDots.add("Previous");
		} else {
			pagingWithDots.add(String.valueOf(currentPage - 1));
		}
		
		for (int i = 1; i <= totalPages; i++) {
			if (i == 1 || i == totalPages || i >= left && i < right) {
				pagingNumber.add(String.valueOf(i));
			}
		}
		for (String i : pagingNumber) {
			if (l > 0) {
				if (Integer.parseInt(i) - l == 2) {
					pagingWithDots.add(String.valueOf(l + 1));
				} else if (Integer.parseInt(i) - l != 1) {
					pagingWithDots.add("...");
				}
			}
			pagingWithDots.add(i);
			l = Integer.parseInt(i);
		}
		if (currentPage == totalPages) {
			pagingWithDots.add("Next");
		} else {
			pagingWithDots.add(String.valueOf(currentPage + 1));
		}

		return pagingWithDots;
	}

	public List<String> paginationOrder(Page<Order> orderPage, int currentPage) {
		int delta = 2;
		int left = currentPage - delta;
		int right = currentPage + delta + 1;
		int l = 0;
		List<String> pagingNumber = new ArrayList<String>();
		int totalPages = orderPage.getTotalPages();
		List<String> pagingWithDots = new ArrayList<String>();
		
		if (currentPage == 1) {
			pagingWithDots.add("Previous");
		} else {
			pagingWithDots.add(String.valueOf(currentPage - 1));
		}
		
		for (int i = 1; i <= totalPages; i++) {
			if (i == 1 || i == totalPages || i >= left && i < right) {
				pagingNumber.add(String.valueOf(i));
			}
		}
		for (String i : pagingNumber) {
			if (l > 0) {
				if (Integer.parseInt(i) - l == 2) {
					pagingWithDots.add(String.valueOf(l + 1));
				} else if (Integer.parseInt(i) - l != 1) {
					pagingWithDots.add("...");
				}
			}
			pagingWithDots.add(i);
			l = Integer.parseInt(i);
		}
		if (currentPage == totalPages) {
			pagingWithDots.add("Next");
		} else {
			pagingWithDots.add(String.valueOf(currentPage + 1));
		}

		return pagingWithDots;
	}

}
