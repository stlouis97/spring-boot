package com.digidinos.shoppingcart;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.digidinos.shoppingcart.entity.Account;
import com.digidinos.shoppingcart.entity.Product;
import com.digidinos.shoppingcart.service.AccountService;
import com.digidinos.shoppingcart.service.ProductService;
import com.github.javafaker.Faker;

@Component
public class MigrateDataCommandRunner implements CommandLineRunner {
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private AccountService accService;
	
	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	public void run(String... args) throws Exception {
		String strArgs = Arrays.stream(args).collect(Collectors.joining("|"));
		logger.info("Application started with arguments:" + strArgs);

		if (strArgs.equals("migrate")) {
			migratedData();
		}
	}

	protected void migratedData() {
		logger.info("Application started migratedData");

		for (int i = 0; i < 100; i++) {
			Account entity = new Account();
			Faker faker = new Faker();
			
			entity.setUserName(faker.leagueOfLegends().champion());
			entity.setActive(true);
			entity.setEncrytedPassword(bCryptPasswordEncoder.encode("123"));
			entity.setUserRole("ROLE_EMPLOYEE");

			accService.addReservation(entity);
		}
	}

}