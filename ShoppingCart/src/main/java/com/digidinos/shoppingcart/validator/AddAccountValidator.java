package com.digidinos.shoppingcart.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.digidinos.shoppingcart.form.AddAccountForm;
import com.digidinos.shoppingcart.service.AddAccountService;

@Component
public class AddAccountValidator implements Validator {
	@Autowired
	AddAccountService accountService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == AddAccountForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AddAccountForm addAccountForm = (AddAccountForm) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty.addAccountForm.userName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "encrytedPassword","NotEmpty.addAccountForm.encrytedPassword");


	}

}
