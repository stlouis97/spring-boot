package com.digidinos.shoppingcart.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.digidinos.shoppingcart.form.ProductForm;
import com.digidinos.shoppingcart.repository.ProductRepository;
 
@Component
public class ProductFormValidator implements Validator {
 
	@Autowired
	private ProductRepository prod;
	
    // Validator này chỉ dùng để kiểm tra class ProductForm.
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == ProductForm.class;
    }
 
    @Override
	public void validate(Object target, Errors errors) {
		ProductForm productForm = (ProductForm) target;

		// Kiểm tra các trường (field) của ProductForm.
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "code", "NotEmpty.productForm.code");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.productForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "NotEmpty.productForm.price");
	}

 
}
