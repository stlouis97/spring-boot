package com.digidinos.shoppingcart.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.digidinos.shoppingcart.form.AccountChangePasswordForm;
import com.digidinos.shoppingcart.service.AddAccountService;

@Component
public class AccountChangePasswordValidator implements Validator{
	
	@Autowired
	AddAccountService accountService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == AccountChangePasswordForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AccountChangePasswordForm accountChangePasswordForm = (AccountChangePasswordForm) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "encrytedPassword", "NotEmpty.accountChangePasswordForm.encrytedPassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "verifyPassword", "NotEmpty.accountChangePasswordForm.verifyPassword");

		if (!accountChangePasswordForm.getEncrytedPassword().equals(accountChangePasswordForm.getVerifyPassword())) {
			errors.rejectValue("verifyPassword", "NotMatches.accountChangePasswordForm.verifyPassword");
		}

		

	}

}
